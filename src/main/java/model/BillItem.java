/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dao.BillDao;
import dao.ProductDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class BillItem {

    private int id; // PK
    private Bill bill; // FK Bill
    private Product product; // FK Product
    private String name; //ชื่อสินค้าในออร์เดอร
    private int amount; //จำนวนสินค้าที่สั่งในออร์เดอร
    private double price; //ราคาสินค้าต่อชิ้น

    
    
    public BillItem(int id, Bill bill, Product product, String name, int amount, double price) {
        this.id = id;
        this.bill = bill;
        this.product = product;
        this.name = name;
        this.amount = amount;
        this.price = price;
    }

    public BillItem(String name, int amount, double price) {
        this.name = name;
        this.amount = amount;
        this.price = price;
    }

    public BillItem(String name, int amount) {
        this.name = name;
        this.amount = amount;
    }

    //เพิ่ม Bill_item ใหม่
    public BillItem(Bill bill, Product product, String name, int amount, double price) {
        this.bill = bill;
        this.product = product;
        this.name = name;
        this.amount = amount;
        this.price = price;
    }

    public BillItem(Product product, int amount) {
        this.product = product;
        this.amount = amount;
    }

    public BillItem(Product product, String name, int amount, double price) {
        this.product = product;
        this.name = name;
        this.amount = amount;
        this.price = price;
    }
    
    

    public BillItem(Product product, int amount, double price) {
        this.product = product;
        this.amount = amount;
        this.price = price;
    }

    public BillItem() {
        //เพิ่ม Bill_item ใหม่
        this.id = -1;
    }

    public BillItem(Bill bill, Product product, String name, double price) {
        this.bill = bill;
        this.product = product;
        this.name = name;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    
    public double getTotal(){
        return amount * product.getPrice();
    }

    @Override
    public String toString() {
        return "Bill_item{" + "id=" + id + ", bill=" + bill + ", product=" + product + ", name=" + name + ", amount=" + amount + ", price=" + price + '}';
    }

    public static BillItem fromRS(ResultSet rs) {
        
        ProductDao ProductDao = new ProductDao(); // new Object ProductDao
        BillDao billDao = new BillDao(); // new Object BillDao
        BillItem bill_item = new BillItem();
        try {
            bill_item.setId(rs.getInt("bill_item_id")); // PK Bill_item

            // FK Bill
            int Bilid = rs.getInt("Bill_id");
            Bill bill = billDao.get(Bilid);
            bill_item.setBill(bill);      
             //FK Product
             int ProId = rs.getInt("product_id"); 
             Product product_item = ProductDao.get(ProId); 
             bill_item.setProduct(product_item);
           
            bill_item.setName(rs.getString("order_item_name"));
            bill_item.setAmount(rs.getInt("order_item_amount"));
            bill_item.setPrice(rs.getDouble("bill_item_price"));

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);

        }

        return bill_item;
    }

}
