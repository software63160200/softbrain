/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class Employee {

    private int id;
    private String name; //ชื่อพนักงาน
    private String gender; //เพศของพนักงาน
    private String tel; //เบอร์โทรพนักงาน
    private double salary; //เงินเดือนพนักงาน

    public Employee(int id, String name,String gender, String tel, double salary) {
        this.id = id;
        this.name = name;
        this.gender=gender;
        this.tel = tel;
        this.salary = salary;
    }

    public Employee(String name, String gender,String tel, double salary) {
        this.id = 1;
        this.name = name;
        this.gender=gender;
        this.tel = tel;
        this.salary = salary;
    }

    public Employee() {
        this.id = -1;
        this.gender = "M";
        this.salary = 0;
        this.name = "";
        this.tel = "";
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", name=" + name + ", gender=" + gender + ", tel=" + tel + ", salary=" + salary + '}';
    }

   
    
    public static Employee fromRS(ResultSet rs) {
        Employee employee = new Employee();
        try {
            employee.setId(rs.getInt("emp_id"));
            employee.setName(rs.getString("emp_name"));
            employee.setGender(rs.getString("emp_gender"));
            employee.setTel(rs.getString("emp_tel"));
            employee.setSalary(rs.getDouble("emp_salary"));
        

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);

        }

        return employee;
    }

}
