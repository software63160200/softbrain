/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class Stock {

    private int id;
    private Date date; //วันที่เช็คสินค้า
    private String employee; // ชื่อพนักงานที่เช็ค

    ArrayList<StockItem> stockDetails; //เอาไว้ Add stockitem

    public Stock(int id, Date date, String employee) {
        this.id = id;
        this.date = date;
        this.employee = employee;
    }

    public Stock(Date date, String employee) {
        this.id = -1;
        this.date = date;
        this.employee = employee;
    }

    public Stock(String empname) {
        this.id = -1;
        stockDetails = new ArrayList<>();
        date = new Date();
        this.employee = empname;
    }

    public Stock() {
         this.id = -1;
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getEmployee() {
        return employee;
    }

    public void setEmployee(String employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "Stock{" + "id=" + id + ", date=" + date + ", employee=" + employee + '}';
    }

    public static Stock fromRS(ResultSet rs) {
        Stock stock = new Stock();
        try {
            stock.setId(rs.getInt("stock_id")); // PK Stock

            // Set Date
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String x = rs.getString("stock_date");
            stock.setDate(sdf.parse(x));

            stock.setEmployee(rs.getString("stock_emp_name"));

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);

        } catch (ParseException ex) {
            Logger.getLogger(Stock.class.getName()).log(Level.SEVERE, null, ex);
        }

        return stock;
    }

    public ArrayList<StockItem> getStockDetails() { // get Detail ของ Stock ออกมา
        return stockDetails;
    }

    public void setStockDetails(ArrayList<StockItem> stockDetails) {
        this.stockDetails = stockDetails; // set ค่าDetail ของ Stock ใหม่
    }

    public void addStockDetail(StockItem stockDetail) { // Add ใส่ทุกตัว
        stockDetails.add(stockDetail);
    }
    
    public void addStockDetail(String nameproduct ,int price ,int amount, Meterial meterial) { // Add 
       StockItem stockItem =new StockItem(nameproduct, price, amount, this, meterial);
       this.addStockDetail(stockItem);
    }

}
