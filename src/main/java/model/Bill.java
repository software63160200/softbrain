/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class Bill {

    private int id; // PK
    private Date date; // วันที่ออร์เดอร
    private int amout; //จำนวนชิ้นที่ซื้อรวมกี่ชิ้น
    private double discount; // ส่วนลด
    private double cash; // จ่าย
    private double change; // เงินทอน
    private double total;// เงินรวม
    private ArrayList<BillItem> billItems;

    public Bill(int id,Date date, int amout, double discount, double cash, double change, double total, ArrayList<BillItem> billItem) {
        this.id = id;
        this.date = date;
        this.amout = amout;
        this.discount = discount;
        this.cash = cash;
        this.change = change;
        this.total = total;
        this.billItems = billItem;
    }

    //เพิ่ม Bill ใหม่
    public Bill(Date date, int amout, double discount, double cash, double change, double total, ArrayList<BillItem> billItem) {
        this.id = -1;
        this.date = date;
        this.amout = amout;
        this.discount = discount;
        this.cash = cash;
        this.change = change;
        this.total = total;
        this.billItems = billItem;
    }
    

    public Bill() { //เพิ่ม Bill ใหม่
        this.id = -1;
        date = new Date();
        discount = 0;
        cash = 0;
        change = 0;
        amout = 0;
        total = 0;
        billItems = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<BillItem> getBillItem() {
        return billItems;
    }

    public void setBillItem(ArrayList<BillItem> billItem) {
        this.billItems = billItem;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getCash() {
        return cash;
    }

    public void setCash(double cash) {
        this.cash = cash;
    }

    public double getChange() {
        return change;
    }

    public void setChange(double change) {
        this.change = change;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public int getAmout() {
        return amout;
    }

    public void setAmout(int amout) {
        this.amout = amout;
    }
    
    
    
    

    @Override
    public String toString() {
        return "Bill{" + "id=" + id + ", date=" + date + ", discount=" + discount + ", cash=" + cash + ", change=" + change +", amout="+amout +", total=" + total + '}';
    }
    
    public void addBillItem(BillItem billItem) {
        billItems.add(billItem);
        total = total + billItem.getTotal();
        amout = amout + billItem.getAmount();     
    }
    
     public void addBillItem(Product product, String name,int amount, double price) {
        BillItem billItem = new BillItem(this,product, name ,amount ,price);
        this.addBillItem(billItem);
    }

    public void addBillItem(Product product, int amount) {
        BillItem billItem = new BillItem(this,product, product.getName(), amount, product.getPrice());
        this.addBillItem(billItem);
    }

    public static Bill fromRS(ResultSet rs) {
        Bill bill = new Bill();

        try {
            bill.setId(rs.getInt("bill_id"));
            // Set Date
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String x = rs.getString("bill_date");
            bill.setDate(sdf.parse(x));
            
            bill.setDiscount(rs.getDouble("bill_discount"));
            bill.setCash(rs.getDouble("bill_cash"));
            bill.setChange(rs.getDouble("bill_change"));
            bill.setAmout(rs.getInt("bill_amount"));
            bill.setTotal(rs.getDouble("bill_total"));

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);

        } catch (ParseException ex) {
            Logger.getLogger(Bill.class.getName()).log(Level.SEVERE, null, ex);
        }

        return bill;
    }

}
