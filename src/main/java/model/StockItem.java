/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;



import dao.MeterialDao;
import dao.StockDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class StockItem {

    private int id; // PK
    private String ProductName; //ชื่อสินค้า
    private double price; // ราคาสินค้าในสต็อก
    private int ProductAmount; // จำนวนที่เช็คได้
    private Stock stock; // FK Stock
    private Meterial meterial; // FK Meterial

    public StockItem(int id, String ProductName, double price, int ProductAmount, Stock stock, Meterial meterial) {
        this.id = id;
        this.ProductName = ProductName;
        this.price = price;
        this.ProductAmount = ProductAmount;
        this.stock = stock;
        this.meterial = meterial;
    }

    // เพิ่มใหม่
    public StockItem(String ProductName, double price, int ProductAmount, Stock stock, Meterial meterial) {
        this.id = -1;
        this.ProductName = ProductName;
        this.price = price;
        this.ProductAmount = ProductAmount;
        this.stock = stock;
        this.meterial = meterial;
    }

    public StockItem() { // เพิ่มใหม่
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String ProductName) {
        this.ProductName = ProductName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getProductAmount() {
        return ProductAmount;
    }

    public void setProductAmount(int ProductAmount) {
        this.ProductAmount = ProductAmount;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public Meterial getMeterial() {
        return meterial;
    }

    public void setMeterial(Meterial meterial) {
        this.meterial = meterial;
    }

    @Override
    public String toString() {
        return "Stock_item{" + "id=" + id + ", ProductName=" + ProductName + ", price=" + price + ", ProductAmount=" + ProductAmount + ", stock=" + stock + ", meterial=" + meterial + '}';
    }

    public static StockItem fromRS(ResultSet rs) {
        StockItem st_item = new StockItem();
        StockDao stockDao = new StockDao();
        MeterialDao meterialDao = new MeterialDao();
        try {
            st_item.setId(rs.getInt("stitem_id")); // PK 

            // FK Stock
            int StockID = rs.getInt("stock_id"); 
            Stock stk_obj = stockDao.get(StockID); 
            st_item.setStock(stk_obj);

            // FK Meterial
            int MetId = rs.getInt("mt_id");
            Meterial met_item = meterialDao.get(MetId);
            st_item.setMeterial(met_item);

            st_item.setProductName(rs.getString("stitem_product_name"));
            st_item.setPrice(rs.getDouble("stitem_price"));
            st_item.setProductAmount(rs.getInt("stitem_amount"));

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);

        }

        return st_item;
    }

}
