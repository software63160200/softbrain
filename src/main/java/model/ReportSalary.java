/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Asus
 */
public class ReportSalary {
    String period;
    double total;

    public ReportSalary(String period, double slr_total) {
        this.period = period;
        this.total = slr_total;
    }

    public ReportSalary() {
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "ReportSalary{" + "period=" + period + ", slr_total=" + total + '}';
    }
    
   public static ReportSalary  fromRS(ResultSet rs) {
                    ReportSalary  obj = new ReportSalary ();
        try {
            
            obj.setPeriod(rs.getString("period"));
            obj.setTotal(rs.getDouble("Cash"));
            return obj;
        } catch (SQLException ex) {
            Logger.getLogger(ReportSalary.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }
}
