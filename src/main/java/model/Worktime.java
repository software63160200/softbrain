/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dao.EmployeeDao;
import dao.SalaryDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class Worktime {
    
    private int id; //PK
//    private Date checkIn; //  วันเวลาเข้างาน
//    private Date checkOut; // วันเวลาออกงาน
    private String checkin;
    private String checkout;
    private int hour; // ชั่วโมงการทำงาน
    private Employee employee; // FK Employee
    private Salary salary;// FK Salary

    public Worktime(int id, String checkIn, String checkOut, int hour, Employee employee, Salary salary) {
        this.id = id;
        this.checkin = checkIn;
        this.checkout = checkOut;
        this.hour = hour;
        this.employee = employee;
        this.salary = salary;
    }
    
    public Worktime(String checkIn, String checkOut, int hour, Employee employee, Salary salary) {
        this.id = -1;
        this.checkin = checkIn;
        this.checkout = checkOut;
        this.hour = hour;
        this.employee = employee;
        this.salary = salary;
    }
    
    public Worktime() {
        this.id = -1;
    }
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getCheckIn() {
        return checkin;
    }
    
    public void setCheckIn(String checkIn) {
        this.checkin = checkIn;
    }
    
    public String getCheckOut() {
        return checkout;
    }
    
    public void setCheckOut(String checkOut) {
        this.checkout = checkOut;
    }
    
    public int getHour() {
        return hour;
    }
    
    public void setHour(int hour) {
        this.hour = hour;
    }
    
    public Employee getEmployee() {
        return employee;
    }
    
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    
    public Salary getSalary() {
        return salary;
    }
    
    public void setSalary(Salary salary) {
        this.salary = salary;
    }
    
    @Override
    public String toString() {
        return "Worktime{" + "id=" + id + ", checkIn=" + checkin + ", checkOut=" + checkout + ", hour=" + hour + ", employee=" + employee + ", salary=" + salary + '}';
    }
    
    public static Worktime fromRS(ResultSet rs) {
        Worktime worktime = new Worktime();
        EmployeeDao empDao = new EmployeeDao();
        SalaryDao salaryDao = new SalaryDao();
        try {
            worktime.setId(rs.getInt("wt_id")); // PK 

            // Set Date checkin
//            SimpleDateFormat sdfIn = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            String DCheckIn = rs.getString("checkin");
//            worktime.setCheckIn(sdfIn.parse(DCheckIn));
            worktime.setCheckIn(rs.getString("checkin"));
            
            worktime.setCheckOut(rs.getString("checkout"));

            // Set Date checkout
//            SimpleDateFormat sdfOut = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            String DCheckOut = rs.getString("checkout");
//            worktime.setCheckOut(sdfOut.parse(DCheckOut));
            worktime.setHour(rs.getInt("wt_hour"));

            // FK Employee
            int EmpId = rs.getInt("emp_id");
            Employee empId = empDao.get(EmpId);
            worktime.setEmployee(empId);

//          FK Salary
            int SaId = rs.getInt("slr_id");
            Salary salaryId = salaryDao.get(SaId);
            worktime.setSalary(salaryId);
            
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            
        }  
        return worktime;
    }
    
}
