/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class Store {
    private int id;
    private String name; //ชื่อร้าน
    private String building; //ตั้งที่อาคาร
    private String faculty; //คณะที่ตั้ง
    private String tel;//เบอ์โทรร้าน

    public Store(int id, String name, String building, String faculty, String tel) {
        this.id = id;
        this.name = name;
        this.building = building;
        this.faculty = faculty;
        this.tel = tel;
    }

    public Store(String name, String building, String faculty, String tel) { // เพิ่ม store ใหม่
        this.id=-1;
        this.name = name;
        this.building = building;
        this.faculty = faculty;
        this.tel = tel;
    }

    public Store() { // เพิ่ม store ใหม่
        this.id=-1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    @Override
    public String toString() {
        return "Store{" + "id=" + id + ", name=" + name + ", building=" + building + ", faculty=" + faculty + ", tel=" + tel + '}';
    }
    
     public static Store fromRS(ResultSet rs) {
        Store store = new Store();
        try {
            store.setId(rs.getInt("store_id"));
            store.setName(rs.getString("store_name"));
            store.setBuilding(rs.getString("store_building"));
            store.setFaculty(rs.getString("store_faculty"));
            store.setTel(rs.getString("store_tel"));

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);

        }

        return store;
    }
    
}
