/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class Meterial {

    private int id;
    private String productName; // ชื่อสินค้า
    private String productUnit; //หน่วยสินค้า
    private int amountIn; // จำนวนสินค้าเข้า
    private int amountOut; // จำนวนสินค้าออก
    private int minimumAmount; //จำนวนสินค้าขั้นต่ำ
    private int amountBalance; // สินค้าคงเหลือ

    public Meterial(int id, String productName, String productUnit, int amountIn, int amountOut, int minimumAmount, int amountBalance) {
        this.id = id;
        this.productName = productName;
        this.productUnit = productUnit;
        this.amountIn = amountIn;
        this.amountOut = amountOut;
        this.minimumAmount = minimumAmount;
        this.amountBalance = amountBalance;
    }

    public Meterial(String productName, String productUnit, int amountIn, int amountOut, int minimumAmount, int amountBalance) {
        this.id=-1;
        this.productName = productName;
        this.productUnit = productUnit;
        this.amountIn = amountIn;
        this.amountOut = amountOut;
        this.minimumAmount = minimumAmount;
        this.amountBalance = amountBalance;
    }

    public Meterial() {
        this.id=-1;
    }

    public int getMinimumAmount() {
        return minimumAmount;
    }

    public void setMinimumAmount(int minimumAmount) {
        this.minimumAmount = minimumAmount;
    }

   

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductUnit() {
        return productUnit;
    }

    public void setProductUnit(String productUnit) {
        this.productUnit = productUnit;
    }

    public int getAmountIn() {
        return amountIn;
    }

    public void setAmountIn(int amountIn) {
        this.amountIn = amountIn;
    }

    public int getAmountOut() {
        return amountOut;
    }

    public void setAmountOut(int amountOut) {
        this.amountOut = amountOut;
    }

    public int getAmountBalance() {
        return amountBalance;
    }

    public void setAmountBalance(int amountBalance) {
        this.amountBalance = amountBalance;
    }

    @Override
    public String toString() {
        return "Meterial{" + "id=" + id + ", productName=" + productName + ", productUnit=" + productUnit + ", amountIn=" + amountIn + ", amountOut=" + amountOut + ", amountBalance=" + amountBalance + '}';
    }
    
     public static Meterial fromRS(ResultSet rs) {
        Meterial meterial = new Meterial();
        try {
            
            meterial.setId(rs.getInt("mt_id")); // PK 
            meterial.setProductName(rs.getString("mt_product_name"));
            meterial.setProductUnit(rs.getString("mt_product_unit"));
            meterial.setAmountIn(rs.getInt("mt_amount_in"));
            meterial.setAmountOut(rs.getInt("mt_amount_out"));
            meterial.setMinimumAmount(rs.getInt("mt_minimum_amout"));
            meterial.setAmountBalance(rs.getInt("mt_amount_balance"));

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);

        }

        return meterial;
    }

}
