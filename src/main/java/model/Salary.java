/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dao.EmployeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class Salary {

    private int id;
    private Date date; // วันที่จ่าย
    private double perh; // เงินต่อชั่วโมง -->45 บาทต่อชั่วโมง
    private double total; // เงินรวม
    private Employee employee; // พนักงาน

    public Salary(int id, Date date, double perh, double total, Employee employee) {
        this.id = id;
        this.date = date;
        this.perh = perh;
        this.total = total;
        this.employee = employee;
    }

    public Salary(Date date, double perh, double total, Employee employee) {
        this.id = -1;
        this.date = date;
        this.perh = perh;
        this.total = total;
        this.employee = employee;
    }

    public Salary() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getPerh() {
        return perh;
    }

    public void setPerh(double perh) {
        this.perh = perh;
    }

 
    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "Salary{" + "id=" + id + ", date=" + date + ", perh=" + perh + ", total=" + total + ", employee=" + employee + '}';
    }

 
    public static Salary fromRS(ResultSet rs) {
        Salary salary = new Salary();
        EmployeeDao empDao = new EmployeeDao();
        try {
            salary.setId(rs.getInt("slr_id")); // PK  Salary

            // Set Date
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String x = rs.getString("slr_date");
            salary.setDate(sdf.parse(x));
//               salary.setDate(rs.getDate("date"));

            salary.setPerh(rs.getDouble("slr_perh"));
            salary.setTotal(rs.getDouble("slr_total"));

            //FK Employee
            int EmpId = rs.getInt("emp_id"); 
            Employee emp_obj = empDao.get(EmpId); 
            salary.setEmployee(emp_obj);

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);

        } catch (ParseException ex) { 
            Logger.getLogger(Salary.class.getName()).log(Level.SEVERE, null, ex);
        }

        return salary;
    }

}
