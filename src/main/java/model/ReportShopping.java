/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class ReportShopping {
     String period;
    double total;
    String employee;

    public ReportShopping(String period, double total, String employee) {
        this.period = period;
        this.total = total;
        this.employee=employee;
    }

    public ReportShopping() {
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getEmployee() {
        return employee;
    }

    public void setEmployee(String employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "ReportShopping{" + "period=" + period + ", total=" + total + ", employee=" + employee + '}';
    }
     public static ReportShopping fromRS(ResultSet rs) {
     
         ReportShopping obj =new ReportShopping();
        try {
            obj.setPeriod(rs.getString("period"));
            obj.setTotal(rs.getDouble("total"));
            obj.setEmployee(rs.getString("Empolyee"));
        } catch (SQLException ex) {
            Logger.getLogger(ReportShopping.class.getName()).log(Level.SEVERE, null, ex);
        }
         return obj;
    }
}
