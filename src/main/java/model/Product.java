/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dao.ProductDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class Product {

    private int id;
    private String name; // ชื้อสินค้าที่ขาย
    private String type; //ประเภทสินค้าที่ขาย
    private String size; //ขนาดเเก้วสินค้า
    private String sweetlevel; //ระดับความหวาน
    private double price; //ราคาสินค้า

    public Product(int id, String name, String type, String size, String sweetlevel, double price) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.size = size;
        this.sweetlevel = sweetlevel;
        this.price = price;
    }

    public Product(String name, String type, String size, String sweetlevel, double price) { // เพิ่ม Product ใหม่
        this.id = -1;
        this.name = name;
        this.type = type;
        this.size = size;
        this.sweetlevel = sweetlevel;
        this.price = price;
    }

    public Product() { // เพิ่ม Product ใหม่
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getSweetlevel() {
        return sweetlevel;
    }

    public void setSweetlevel(String sweetlevel) {
        this.sweetlevel = sweetlevel;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", type=" + type + ", size=" + size + ", sweetlevel=" + sweetlevel + ", price=" + price + '}';
    }

    public static Product fromRS(ResultSet rs) {
        Product product = new Product();
        try {
            product.setId(rs.getInt("product_id"));
            product.setName(rs.getString("product_name"));
            product.setPrice(rs.getDouble("product_price"));
            product.setSize(rs.getString("product_size"));
            product.setSweetlevel("product_sweetlevel");
            product.setType("product_type");

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);

        }
        return product;
    }
    
    public static ArrayList<Product> getMockProductList(){
        ProductDao productDao = new ProductDao();
        ArrayList<Product> list = new ArrayList<>();
        for(Product product : productDao.getAll()){
            list.add(product);
        }
        return list;
    }
}
