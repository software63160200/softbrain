/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author acer
 */
public class Receipt {

    private int id; // PK
    private Date date; // วันที่ซื้อของ
    private double cash; //จ่ายเงินเท่าไร
    private double change; //เงินทอนเท่าไร
    private double total; // เงินรวมที่ของที่ซื้อ
    private String employee; //ชื่อคนที่ซื้อ
    ArrayList<ReceiptItem> receiptItem;

    public Receipt(int id, Date date, double cash, double change, double total, String employee) {
        this.id = id;
        this.date = date;
        this.cash = cash;
        this.change = change;
        this.total = total;
        this.employee = employee;
    }

    public Receipt(Date date, double cash, double change, double total, String employee) {
        this.id = -1;
        this.date = date;
        this.cash = cash;
        this.change = change;
        this.total = total;
        this.employee = employee;
    }

    public Receipt() {
        this.id = -1;
        total = 0;
        date = new Date();
        receiptItem = new ArrayList<>();
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getCash() {
        return cash;
    }

    public void setCash(double cash) {
        this.cash = cash;
    }

    public double getChange() {
        return change;
    }

    public void setChange(double change) {
        this.change = change;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getEmp_name() {
        return employee;
    }

    public void setEmployee(String employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "Receipt{" + "id=" + id + ", date=" + date + ", cash=" + cash + ", change=" + change + ", total=" + total + ", employee=" + employee + '}';
    }
    
     public static Receipt fromRS(ResultSet rs) {
        Receipt receipt = new Receipt();
        try {
            receipt.setId(rs.getInt("rcp_id")); // PK  Receipt

            // Set Date
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String x = rs.getString("rcp_date");
            receipt.setDate(sdf.parse(x));

          
            receipt.setCash(rs.getDouble("rcp_cash"));
            receipt.setChange(rs.getDouble("rcp_change"));
            receipt.setTotal(rs.getDouble("rcp_total"));
            receipt.setEmployee(rs.getString("emp_name"));

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);

        } catch (ParseException ex) {
            Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
        }

        return receipt;
    }

    public ArrayList<ReceiptItem> getReceiptItem() {
        return receiptItem;
    }

}
