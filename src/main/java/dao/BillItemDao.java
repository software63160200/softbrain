/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.BillItem;

/**
 *
 * @author acer
 */
public class BillItemDao implements Dao<BillItem>{

    @Override
    public BillItem get(int id) {
        BillItem billitem = null;
        String sql = "SELECT * FROM Bill_items WHERE bill_item_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                billitem = billitem.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return billitem;
    }

    @Override
    public List<BillItem> getAll() {
        ArrayList<BillItem> list = new ArrayList();
        String sql = "SELECT * FROM Bill_items";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BillItem billitem = BillItem.fromRS(rs);
                list.add(billitem);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public BillItem save(BillItem obj) {
        String sql = "INSERT INTO Bill_items (bill_id,product_id,order_item_name,order_item_amount,bill_item_price)"
                + "VALUES(?, ?, ?, ?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getBill().getId());
            stmt.setInt(2, obj.getProduct().getId());
            stmt.setString(3, obj.getName());
            stmt.setInt(4, obj.getAmount());
            stmt.setDouble(5, obj.getPrice());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public BillItem update(BillItem obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(BillItem obj) {
        String sql = "DELETE FROM Bill_items WHERE bill_item_id=?";
        Connection conn = DatabaseHelper.getConnect();
        
        try
        {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex)
        {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<BillItem> getAll(String where, String order) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
}
