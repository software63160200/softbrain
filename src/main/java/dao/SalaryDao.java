/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.util.List;
import model.Salary;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.ReportSalary;

/**
 *
 * @author acer
 */
public class SalaryDao implements Dao<Salary>{

    @Override
    public Salary get(int id) {
        Salary salary = null;
        String sql = "SELECT * FROM salary WHERE slr_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                salary = Salary.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return salary;
    }

    @Override
    public List<Salary> getAll() {
        ArrayList<Salary> list = new ArrayList();
        String sql = "SELECT * FROM Salary";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Salary salary = Salary.fromRS(rs);
                list.add(salary);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
   

    @Override
    public Salary save(Salary obj) {
        String sql = "INSERT INTO Salary (slr_total, emp_id)"
                + "VALUES(?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, String.valueOf(obj.getTotal()));
            stmt.setString(2, String.valueOf(obj.getEmployee().getId()));
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Salary update(Salary obj) {
        String sql = "UPDATE Salary"
                + " SET slr_total = ?, emp_id = ?"
                + " WHERE emp_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, String.valueOf(obj.getTotal()));
            stmt.setString(2, String.valueOf(obj.getEmployee().getId()));
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Salary obj) {
        String sql = "DELETE FROM Salary WHERE slr_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1; 
    }

    @Override
    public List<Salary> getAll(String where, String order) {
        return null;
    }
    
    public ArrayList<ReportSalary> getMonthReportSalary(int month) {
        ArrayList<ReportSalary> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%m\", slr_date) as period, SUM(slr_total) as slr_total FROM Salary\n"
                + "WHERE strftime(\"%m\", slr_date) = \"" + month + "\"\n"
                + "GROUP BY period\n"
                + "ORDER BY period DESC\n"
                + "";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportSalary item = ReportSalary.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public ArrayList<ReportSalary> getYearReportSalary(int year) {
        ArrayList<ReportSalary> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y\", slr_date) as period, SUM(slr_total) as slr_total FROM Salary\n"
                + "WHERE strftime(\"%Y\", slr_date) = \"" + year + "\"\n"
                + "GROUP BY period\n"
                + "ORDER BY period DESC"
                + "";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportSalary item = ReportSalary.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<Salary> getAll(String order) {
        ArrayList<Salary> list = new ArrayList();
        String sql = "SELECT * FROM Salary ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Salary user = Salary.fromRS(rs);
                list.add(user);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
}
