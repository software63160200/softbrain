/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.ReportShopping;

/**
 *
 * @author acer
 */
public class ReportShoppingDao {

    public List<ReportShopping> getDayShopReport() {
        ArrayList<ReportShopping> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%m-%d\",rcp_date) as Period ,rcp_total as Total ,emp_name as Empolyee\n"
                + "FROM Receipt\n"
                + "GROUP BY Period\n"
                + "ORDER BY Period ASC;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportShopping item = ReportShopping.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }
    public List<ReportShopping> getMonthShopReport() {
        ArrayList<ReportShopping> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%m\",rcp_date) as Period ,rcp_total as Total ,emp_name as Empolyee\n"
                + "FROM Receipt\n"
                + "GROUP BY Period\n"
                + "ORDER BY Period ASC;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportShopping item = ReportShopping.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }
    
    public List<ReportShopping> getYearShopReport() {
        ArrayList<ReportShopping> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y\",rcp_date) as Period ,rcp_total as Total ,emp_name as Empolyee\n"
                + "FROM Receipt\n"
                + "GROUP BY Period\n"
                + "ORDER BY Period ASC;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportShopping item = ReportShopping.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }

}
