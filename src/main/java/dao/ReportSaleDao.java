/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.ReportSale;

/**
 *
 * @author Asus
 */
public class ReportSaleDao {

    public List<ReportSale> getDayReport() {
        ArrayList<ReportSale> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%m-%d\",bill_date) as Period ,sum(bill_total) as Total\n"
                + "FROM Bill\n"
                + "GROUP BY Period\n"
                + "ORDER BY Period ASC;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportSale item = ReportSale.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReportSale> getMonthReport() {
        ArrayList<ReportSale> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%m\",bill_date) as Period ,sum(bill_total) as Total\n"
                + "FROM Bill\n"
                + "GROUP BY Period\n"
                + "ORDER BY Period ASC;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportSale item = ReportSale.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReportSale> getYearReport() {
        ArrayList<ReportSale> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y\",bill_date) as Period ,sum(bill_total) as Total\n"
                + "FROM Bill\n"
                + "GROUP BY Period\n"
                + "ORDER BY Period ASC;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportSale item = ReportSale.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
