/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Customers;

/**
 *
 * @author acer
 */
public class CustomerDao implements Dao<Customers>{

    @Override
    public Customers get(int id) {
        Customers customers = null;
        String sql = "SELECT * FROM Customers WHERE cus_id=?";
        Connection conn = DatabaseHelper.getConnect();
        
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                customers = Customers.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        return customers;
        
    }

    @Override
    public List<Customers> getAll() {
        ArrayList<Customers> list = new ArrayList();
        String sql = "SELECT * FROM Customers";
        Connection conn = DatabaseHelper.getConnect();
        
        try
        { 
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next())
            {
                Customers item = Customers.fromRS(rs);
                list.add(item);

            }
        } catch (SQLException ex)
        {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Customers save(Customers obj) {
        String sql = "INSERT INTO Customers (cus_name, cus_status, cus_tel, cus_points) VALUES(?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try
        {    
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,obj.getName());
            stmt.setString(2,obj.getStatus());
            stmt.setString(3,obj.getTel());
            stmt.setInt(4,obj.getPoints());
            
             stmt.executeUpdate(); // หายย

            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex)
        {
            System.out.println(ex.getMessage());
        }
        return obj;
    }

    @Override
    public Customers update(Customers obj) {
        String sql = "UPDATE Customers"
                + " SET cus_name = ?, cus_status = ?, cus_tel = ?, cus_points = ?"
                + " WHERE cus_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try
        {    
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,obj.getName());
            stmt.setString(2,obj.getStatus());
            stmt.setString(3,obj.getTel());
            stmt.setInt(4,obj.getPoints());
            stmt.setInt(5, obj.getId());
            int ret = stmt.executeUpdate();
            return obj;
        } catch (SQLException ex)
        {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Customers obj) {
        String sql = "DELETE FROM Customers WHERE cus_id=?";
        Connection conn = DatabaseHelper.getConnect();
        
        try
        {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex)
        {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
   public List<Customers> getAll(String where, String order) {
        ArrayList<Customers> list = new ArrayList();
        String sql = "SELECT * FROM Customers where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Customers cus = Customers.fromRS(rs);
                list.add(cus);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
   public List<Customers> getAll(String order) {
        ArrayList<Customers> list = new ArrayList();
        String sql = "SELECT * FROM Customers ORDER BY "+ order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Customers cus = Customers.fromRS(rs);
                list.add(cus);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    public Customers getByTel(String Tel) {
        Customers customer = null;
        String sql = "SELECT * FROM Customers WHERE cus_tel LIKE ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, "%" +Tel+ "%");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                customer = Customers.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return customer;
    }
   

    
}
