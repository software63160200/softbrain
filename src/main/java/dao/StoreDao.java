/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Store;

/**
 *
 * @author acer
 */
public class StoreDao implements Dao<Store>{

    @Override
    public Store get(int id) {
        Store store = new Store();
        String sql = "SELECT * FROM Store WHERE store_id=?";
        Connection conn = DatabaseHelper.getConnect();
        
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                store = Store.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        return store;
    }

    @Override
    public List<Store> getAll() {
        ArrayList<Store> list = new ArrayList();
        String sql = "SELECT * FROM Store";
        Connection conn = DatabaseHelper.getConnect();
        try
        {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next())
            {
                Store item = Store.fromRS(rs);
                list.add(item);

            }
        } catch (SQLException ex)
        {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Store save(Store obj) {
        String sql = "INSERT INTO Store (store_name, store_building, store_faculty, store_tel) VALUES(?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try
        {    
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,obj.getName());
            stmt.setString(2,obj.getBuilding());
            stmt.setString(3,obj.getFaculty());
            stmt.setString(4,obj.getTel());
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex)
        {
            System.out.println(ex.getMessage());
        }
        return obj;
    }

    @Override
    public Store update(Store obj) {
        String sql = "UPDATE Store"
                + " store_name = ?, store_building = ?, store_faculty = ?, store_tel = ?"
                + " WHERE store_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try
        {    
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,obj.getName());
            stmt.setString(2,obj.getBuilding());
            stmt.setString(3,obj.getFaculty());
            stmt.setString(4,obj.getTel());
            stmt.setInt(5, obj.getId());
            int ret = stmt.executeUpdate();
            return obj;
        } catch (SQLException ex)
        {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Store obj) {
        String sql = "DELETE FROM Store WHERE store_id=?";
        Connection conn = DatabaseHelper.getConnect();
        
        try
        {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex)
        {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Store> getAll(String where, String order) {
        return null;
    }
    
}
