/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.ReportSalary;

/**
 *
 * @author acer
 */
public class ReportSalaryDao {

    public List<ReportSalary> getDaySalaryReport() {
        ArrayList<ReportSalary> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%m-%d\",slr_date) as Period ,SUM(slr_total) as Cash\n"
                + "FROM Salary\n"
                + "LEFT JOIN Worktime\n"
                + "ON Salary.slr_id= Worktime.wt_id\n"
                + "GROUP BY Period\n"
                + "ORDER BY Period ASC";
        Connection conn = DatabaseHelper.getConnect();

        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportSalary item = ReportSalary.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<ReportSalary> getMounthSalaryReport() {
        ArrayList<ReportSalary> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%m\",slr_date) as Period ,SUM(slr_total) as Cash\n"
                + "FROM Salary\n"
                + "LEFT JOIN Worktime\n"
                + "ON Salary.slr_id= Worktime.wt_id\n"
                + "GROUP BY Period\n"
                + "ORDER BY Period ASC";
        Connection conn = DatabaseHelper.getConnect();

        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportSalary item = ReportSalary.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    public List<ReportSalary> getYearSalaryReport() {
        ArrayList<ReportSalary> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y\",slr_date) as Period ,SUM(slr_total) as Cash\n"
                + "FROM Salary\n"
                + "LEFT JOIN Worktime\n"
                + "ON Salary.slr_id= Worktime.wt_id\n"
                + "GROUP BY Period\n"
                + "ORDER BY Period ASC";
        Connection conn = DatabaseHelper.getConnect();

        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportSalary item = ReportSalary.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    
    
}
