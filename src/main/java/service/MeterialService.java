/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.MeterialDao;
import java.util.List;
import model.Meterial;

/**
 *
 * @author acer
 */
public class MeterialService {
    public List<Meterial> getMet() {
        MeterialDao metDao = new MeterialDao();
        return metDao.getAll("mt_id asc");
        
    }

    public Meterial addNew(Meterial editedMet) {
       MeterialDao metDao = new MeterialDao();
        return metDao.save(editedMet);
    }
    
    public Meterial update(Meterial editedMet) {
       MeterialDao metDao = new MeterialDao();
        return metDao.update(editedMet);
    }

    public int delete(Meterial editedMet) {
       MeterialDao metDao = new MeterialDao();
        return metDao.delete(editedMet);
    }
}
