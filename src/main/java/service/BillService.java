/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.BillDao;
import dao.BillItemDao;
import dao.MeterialDao;
import java.util.List;
import model.Bill;
import model.BillItem;

/**
 *
 * @author acer
 */
public class BillService {
    public List<Bill> getMet() {
      
        BillDao billDao = new BillDao();
        return billDao.getAll();
        
    }

    public Bill addNew(Bill editedBill) {
        BillDao billDao = new  BillDao();
        return billDao.save(editedBill);
    }
    
    public Bill update(Bill editedBill) {
       BillDao billDao = new  BillDao();
        return billDao.update(editedBill);
    }

    public int delete(Bill editedMet) {
        BillDao billDao = new  BillDao();
        return billDao.delete(editedMet);
    }
}
