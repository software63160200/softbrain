/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;


import dao.CustomerDao;
import java.util.List;
import model.Customers;

/**
 *
 * @author acer
 */
public class CustomerService {
    
    public List<Customers> getCus() {
        CustomerDao cusDao = new CustomerDao();
        return cusDao.getAll("cus_id asc");
        
    }

    public Customers addNew(Customers editedCus) {
       CustomerDao userDao = new CustomerDao();
       return userDao.save(editedCus);
    }

    public Customers update(Customers editedCus) {
       CustomerDao userDao = new CustomerDao();
       return userDao.update(editedCus);
    }

    public int delete(Customers editedCus) {
       CustomerDao userDao = new CustomerDao();
       return userDao.delete(editedCus);
    }
    
}
