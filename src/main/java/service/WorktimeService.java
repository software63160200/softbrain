/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.WorktimeDao;
import java.util.List;
import model.Worktime;

/**
 *
 * @author user
 */
public class WorktimeService {
    
    public List<Worktime> getWorktime(){
        WorktimeDao worktimeDao = new WorktimeDao();
        return worktimeDao.getAll();
    } 
    
    
}
