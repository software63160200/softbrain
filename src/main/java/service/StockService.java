/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;


import dao.StockItemDao;
import java.util.List;
import model.StockItem;

/**
 *
 * @author acer
 */
public class StockService {
    
    public List<StockItem> getSt() {
        StockItemDao stitemDao = new StockItemDao();
        return stitemDao.getAll("stitem_id asc");
        
    }

    public StockItem addNew(StockItem editedSt) {
       StockItemDao stitemDao = new StockItemDao();
        return stitemDao.save(editedSt);
    }
    
    public StockItem update(StockItem editedSt) {
       StockItemDao stitemDao = new StockItemDao();
        return stitemDao.update(editedSt);
    }

    public int delete(StockItem editedSt) {
   StockItemDao stitemDao = new StockItemDao();
        return stitemDao.delete(editedSt);
    }
}
