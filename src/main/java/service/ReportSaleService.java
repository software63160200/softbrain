/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.ReportSaleDao;
import java.util.List;
import model.ReportSale;

/**
 *
 * @author Asus
 */
public class ReportSaleService {
    public List<ReportSale> getReportSaleByDay(){
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getDayReport();
    }
    
    public List<ReportSale> getReportSaleByMonth(){
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getMonthReport();
    }
    
    public List<ReportSale> getReportSaleByYear(){
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getYearReport();
    }
    
}
