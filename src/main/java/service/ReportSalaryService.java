/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.ReportSalaryDao;
import dao.SalaryDao;
import java.util.List;
import model.ReportSalary;

/**
 *
 * @author Asus
 */
public class ReportSalaryService {
    
    public List<ReportSalary> getReportSalaryByDay(){
        ReportSalaryDao dao = new ReportSalaryDao();
        return dao.getDaySalaryReport();
    }
    
    public List<ReportSalary> getReportSalaryByMonth(){
        ReportSalaryDao dao = new ReportSalaryDao();
        return dao.getMounthSalaryReport();
    }
    public List<ReportSalary> getReportSalaryByYear(){
        ReportSalaryDao dao = new ReportSalaryDao();
        return dao.getYearSalaryReport();
    }
}
