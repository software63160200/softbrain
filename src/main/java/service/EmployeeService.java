/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.EmployeeDao;
import java.util.List;
import model.Employee;

/**
 *
 * @author user
 */
public class EmployeeService {
    
    public List<Employee> getEmployee() {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.getAll(" emp_name asc");
        
    }

    public Employee addNew(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.save(editedEmployee);
    }
    
    public Employee update(Employee editedEmp) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.update(editedEmp);
    }

    public int delete(Employee editedEmp) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.delete(editedEmp);
    }
}
