/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;


import dao.ReceiptItemDao;
import java.util.List;
import model.ReceiptItem;

/**
 *
 * @author acer
 */
public class ReceiptService {
     public List<ReceiptItem> getRec() {
        ReceiptItemDao rectiemDao = new ReceiptItemDao();
        return rectiemDao.getAll();
        
    }

    public ReceiptItem addNew(ReceiptItem editedMet) {
       ReceiptItemDao metDao = new ReceiptItemDao();
        return metDao.save(editedMet);
    }
    
    public ReceiptItem update(ReceiptItem editedMet) {
       ReceiptItemDao rectiemDao = new ReceiptItemDao();
        return rectiemDao.update(editedMet);
    }

    public int delete(ReceiptItem editedMet) {
       ReceiptItemDao rectiemDao = new ReceiptItemDao();
        return rectiemDao.delete(editedMet);
    }
}
