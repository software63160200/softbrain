/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.SalaryDao;
import java.util.List;
import model.Salary;

/**
 *
 * @author user
 */
public class SalaryService {
    SalaryDao salaryDao = new SalaryDao();
    
    public List<Salary> getWorktime(){
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.getAll();
    } 
     public Salary addNew(Salary editedSalary) {
        SalaryDao SalaryDao = new SalaryDao();
        return SalaryDao.save(editedSalary);
    }
    
    public Salary update(Salary editedSalary) {
        SalaryDao empDao = new SalaryDao();
        return empDao.update(editedSalary);
    }

    public int delete(Salary editedSalary) {
        SalaryDao empDao = new SalaryDao();
        return empDao.delete(editedSalary);
    }
    
}
