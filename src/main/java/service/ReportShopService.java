/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.ReportShoppingDao;
import java.util.List;
import model.ReportShopping;

/**
 *
 * @author acer
 */
public class ReportShopService {

    public List<ReportShopping> getreportShopByDay() {
        ReportShoppingDao dao = new ReportShoppingDao();
        return dao.getDayShopReport();
    }

    public List<ReportShopping> getreportShopByMouth() {
        ReportShoppingDao dao = new ReportShoppingDao();
        return dao.getMonthShopReport();
    }
    
    public List<ReportShopping> getreportShopByYear() {
        ReportShoppingDao dao = new ReportShoppingDao();
        return dao.getYearShopReport();
    }

}
