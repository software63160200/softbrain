/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package ui;

import model.Product;

/**
 *
 * @author acer
 */
public interface OnByProductListener {
    public void buy(Product product,int amount,int price);
}
