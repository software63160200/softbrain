/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package ui;

import java.util.ArrayList;
import javax.swing.ImageIcon;
import model.Product;

/**
 *
 * @author acer
 */
public class PanelProduct extends javax.swing.JPanel {
    private int price;
    private int amount;
    ArrayList<OnByProductListener> subcriberList = new ArrayList<>();
    private final Product product;
    /**
     * Creates new form PanelProduct
     */
    public PanelProduct(Product product) {
        initComponents();
        this.product = product;
        txtNameCof.setText(product.getName() + " " + product.getPrice());
        amount = 1;
       
        if(product.getId()==1){
             btnCoffee.setIcon(new ImageIcon("./Product/1.png"));
        }if(product.getId()==2){
             btnCoffee.setIcon(new ImageIcon("./Product/2.png"));
        }if(product.getId()==3){
             btnCoffee.setIcon(new ImageIcon("./Product/3.png"));
        }if(product.getId()==4){
             btnCoffee.setIcon(new ImageIcon("./Product/4.png"));
        }if(product.getId()==5){
             btnCoffee.setIcon(new ImageIcon("./Product/5.png"));
        }if(product.getId()==6){
             btnCoffee.setIcon(new ImageIcon("./Product/6.png"));
        }if(product.getId()==7){
             btnCoffee.setIcon(new ImageIcon("./Product/7.png"));
        }if(product.getId()==8){
             btnCoffee.setIcon(new ImageIcon("./Product/8.png"));
        }if(product.getId()==9){
             btnCoffee.setIcon(new ImageIcon("./Product/9.png"));
        }if(product.getId()==10){
             btnCoffee.setIcon(new ImageIcon("./Product/10.png"));
        }
      
           
        
        
    }

     public void addOnByProductListener(OnByProductListener subcribe){
         subcriberList.add(subcribe);
     }



    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnCoffee = new javax.swing.JButton();
        txtNameCof = new javax.swing.JLabel();
        btnDel = new javax.swing.JButton();
        txtAmount = new javax.swing.JTextField();
        btnInc = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(106, 52, 48), 2));

        btnCoffee.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 2));
        btnCoffee.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCoffeeActionPerformed(evt);
            }
        });

        txtNameCof.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtNameCof.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtNameCof.setText("Coffee  10");

        btnDel.setBackground(new java.awt.Color(106, 52, 48));
        btnDel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnDel.setForeground(new java.awt.Color(255, 255, 255));
        btnDel.setText("-");
        btnDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDelActionPerformed(evt);
            }
        });

        txtAmount.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtAmount.setText("1");
        txtAmount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAmountActionPerformed(evt);
            }
        });

        btnInc.setBackground(new java.awt.Color(106, 52, 48));
        btnInc.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnInc.setForeground(new java.awt.Color(255, 255, 255));
        btnInc.setText("+");
        btnInc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIncActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnCoffee, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtNameCof, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnDel, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtAmount)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnInc, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnCoffee, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtNameCof, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnDel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtAmount)
                    .addComponent(btnInc, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(18, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnCoffeeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCoffeeActionPerformed
        for( OnByProductListener s: subcriberList){
            s.buy(product, amount,price);
        }
    }//GEN-LAST:event_btnCoffeeActionPerformed

    private void btnDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDelActionPerformed
        if(amount==1 ) return;
        amount--;
        txtAmount.setText(""+amount);
    }//GEN-LAST:event_btnDelActionPerformed

    private void btnIncActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIncActionPerformed
        amount++;
        txtAmount.setText(""+amount);
    }//GEN-LAST:event_btnIncActionPerformed

    private void txtAmountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAmountActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtAmountActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCoffee;
    private javax.swing.JButton btnDel;
    private javax.swing.JButton btnInc;
    private javax.swing.JTextField txtAmount;
    private javax.swing.JLabel txtNameCof;
    // End of variables declaration//GEN-END:variables
}
