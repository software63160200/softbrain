--
-- File generated with SQLiteStudio v3.3.3 on อ. ต.ค. 4 18:08:26 2022
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: Customers
DROP TABLE IF EXISTS Customers;

CREATE TABLE Customers (
    cus_id     INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    cus_name   TEXT (50) NOT NULL,
    cus_status TEXT (10) NOT NULL,
    cus_tel    TEXT (10) NOT NULL,
    cus_points INTEGER   NOT NULL
);

INSERT INTO Customers (
                          cus_id,
                          cus_name,
                          cus_status,
                          cus_tel,
                          cus_points
                      )
                      VALUES (
                          1,
                          'Coffee Inthanin',
                          'Member',
                          '0385478512',
                          9
                      );

INSERT INTO Customers (
                          cus_id,
                          cus_name,
                          cus_status,
                          cus_tel,
                          cus_points
                      )
                      VALUES (
                          2,
                          'Noohin Lakorn',
                          'Member',
                          '0326584596',
                          11
                      );

INSERT INTO Customers (
                          cus_id,
                          cus_name,
                          cus_status,
                          cus_tel,
                          cus_points
                      )
                      VALUES (
                          3,
                          'Jaodang Rangrit',
                          'notMember',
                          '0865412584',
                          0
                      );

INSERT INTO Customers (
                          cus_id,
                          cus_name,
                          cus_status,
                          cus_tel,
                          cus_points
                      )
                      VALUES (
                          4,
                          'Mon Pheunjalakorn',
                          'Member',
                          '0945325684',
                          12
                      );

INSERT INTO Customers (
                          cus_id,
                          cus_name,
                          cus_status,
                          cus_tel,
                          cus_points
                      )
                      VALUES (
                          5,
                          'Promwiharn Klubhua',
                          'notMember',
                          '0953842659',
                          0
                      );

INSERT INTO Customers (
                          cus_id,
                          cus_name,
                          cus_status,
                          cus_tel,
                          cus_points
                      )
                      VALUES (
                          6,
                          'Monsuk Kangmungkoy',
                          'Member',
                          '0355978451',
                          5
                      );

INSERT INTO Customers (
                          cus_id,
                          cus_name,
                          cus_status,
                          cus_tel,
                          cus_points
                      )
                      VALUES (
                          7,
                          'Carrot Dekdoi',
                          'Member',
                          '0356658452',
                          7
                      );

INSERT INTO Customers (
                          cus_id,
                          cus_name,
                          cus_status,
                          cus_tel,
                          cus_points
                      )
                      VALUES (
                          8,
                          'Pornsawan Sawangha',
                          'Member',
                          '0356984512',
                          15
                      );

INSERT INTO Customers (
                          cus_id,
                          cus_name,
                          cus_status,
                          cus_tel,
                          cus_points
                      )
                      VALUES (
                          9,
                          'Boonmee Kumbang',
                          'notMember',
                          '0945214535',
                          0
                      );

INSERT INTO Customers (
                          cus_id,
                          cus_name,
                          cus_status,
                          cus_tel,
                          cus_points
                      )
                      VALUES (
                          10,
                          'Kongkaew Kongjai',
                          'notMember',
                          '0861579346',
                          0
                      );


-- Table: Employees
DROP TABLE IF EXISTS Employees;

CREATE TABLE Employees (
    emp_id     INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    emp_name   TEXT (50) NOT NULL,
    emp_tel    TEXT (10) NOT NULL,
    emp_salary DOUBLE    NOT NULL
                         DEFAULT (0) 
);

INSERT INTO Employees (
                          emp_id,
                          emp_name,
                          emp_tel,
                          emp_salary
                      )
                      VALUES (
                          1,
                          'Noodee Jitjamsai',
                          '098-945-2234',
                          '8,000.00'
                      );

INSERT INTO Employees (
                          emp_id,
                          emp_name,
                          emp_tel,
                          emp_salary
                      )
                      VALUES (
                          2,
                          'Pungyen Nomsod',
                          '099-897-0923',
                          '8,000.00'
                      );

INSERT INTO Employees (
                          emp_id,
                          emp_name,
                          emp_tel,
                          emp_salary
                      )
                      VALUES (
                          3,
                          'Salaloy Puanlom',
                          '079-234-1345',
                          '8,000.00'
                      );


-- Table: Meterial
DROP TABLE IF EXISTS Meterial;

CREATE TABLE Meterial (
    mt_id           INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    mt_product_name TEXT (50) UNIQUE,
    mt_amount       INTEGER   DEFAULT (0) 
                              NOT NULL
);

INSERT INTO Meterial (
                         mt_id,
                         mt_product_name,
                         mt_amount
                     )
                     VALUES (
                         1,
                         'Cocoa powder',
                         8
                     );

INSERT INTO Meterial (
                         mt_id,
                         mt_product_name,
                         mt_amount
                     )
                     VALUES (
                         2,
                         'Coffee beans',
                         9
                     );

INSERT INTO Meterial (
                         mt_id,
                         mt_product_name,
                         mt_amount
                     )
                     VALUES (
                         3,
                         'Thai tea powder',
                         6
                     );

INSERT INTO Meterial (
                         mt_id,
                         mt_product_name,
                         mt_amount
                     )
                     VALUES (
                         4,
                         'Green tea powder',
                         9
                     );

INSERT INTO Meterial (
                         mt_id,
                         mt_product_name,
                         mt_amount
                     )
                     VALUES (
                         5,
                         'Milk',
                         8
                     );

INSERT INTO Meterial (
                         mt_id,
                         mt_product_name,
                         mt_amount
                     )
                     VALUES (
                         6,
                         'Creamer',
                         7
                     );

INSERT INTO Meterial (
                         mt_id,
                         mt_product_name,
                         mt_amount
                     )
                     VALUES (
                         7,
                         'Sugar',
                         7
                     );

INSERT INTO Meterial (
                         mt_id,
                         mt_product_name,
                         mt_amount
                     )
                     VALUES (
                         8,
                         'Condensed milk',
                         9
                     );

INSERT INTO Meterial (
                         mt_id,
                         mt_product_name,
                         mt_amount
                     )
                     VALUES (
                         9,
                         'Syrup',
                         9
                     );


-- Table: Meterial_items
DROP TABLE IF EXISTS Meterial_items;

CREATE TABLE Meterial_items (
    meitem_id         INTEGER  PRIMARY KEY ASC AUTOINCREMENT,
    meitem_stock_date DATETIME NOT NULL
                               DEFAULT (DATETIME('now', 'localtime') ),
    meitem_min_amount INTEGER  NOT NULL
                               DEFAULT (10),
    stock_id          INTEGER  REFERENCES Stock (stock_id) ON DELETE CASCADE
                                                           ON UPDATE CASCADE,
    mt_id             INTEGER  REFERENCES Meterial (mt_id) ON DELETE CASCADE
                                                           ON UPDATE CASCADE
);

INSERT INTO Meterial_items (
                               meitem_id,
                               meitem_stock_date,
                               meitem_min_amount,
                               stock_id,
                               mt_id
                           )
                           VALUES (
                               1,
                               '2022-10-04 16:27:21',
                               10,
                               1,
                               1
                           );

INSERT INTO Meterial_items (
                               meitem_id,
                               meitem_stock_date,
                               meitem_min_amount,
                               stock_id,
                               mt_id
                           )
                           VALUES (
                               2,
                               '2022-10-04 16:27:21',
                               10,
                               2,
                               2
                           );

INSERT INTO Meterial_items (
                               meitem_id,
                               meitem_stock_date,
                               meitem_min_amount,
                               stock_id,
                               mt_id
                           )
                           VALUES (
                               3,
                               '2022-10-04 16:27:21',
                               10,
                               3,
                               3
                           );

INSERT INTO Meterial_items (
                               meitem_id,
                               meitem_stock_date,
                               meitem_min_amount,
                               stock_id,
                               mt_id
                           )
                           VALUES (
                               4,
                               '2022-10-04 16:27:21',
                               10,
                               4,
                               4
                           );

INSERT INTO Meterial_items (
                               meitem_id,
                               meitem_stock_date,
                               meitem_min_amount,
                               stock_id,
                               mt_id
                           )
                           VALUES (
                               5,
                               '2022-10-04 16:27:21',
                               10,
                               5,
                               5
                           );

INSERT INTO Meterial_items (
                               meitem_id,
                               meitem_stock_date,
                               meitem_min_amount,
                               stock_id,
                               mt_id
                           )
                           VALUES (
                               6,
                               '2022-10-04 16:27:21',
                               10,
                               6,
                               6
                           );

INSERT INTO Meterial_items (
                               meitem_id,
                               meitem_stock_date,
                               meitem_min_amount,
                               stock_id,
                               mt_id
                           )
                           VALUES (
                               7,
                               '2022-10-04 16:27:21',
                               10,
                               7,
                               7
                           );

INSERT INTO Meterial_items (
                               meitem_id,
                               meitem_stock_date,
                               meitem_min_amount,
                               stock_id,
                               mt_id
                           )
                           VALUES (
                               8,
                               '2022-10-04 16:27:21',
                               10,
                               8,
                               8
                           );

INSERT INTO Meterial_items (
                               meitem_id,
                               meitem_stock_date,
                               meitem_min_amount,
                               stock_id,
                               mt_id
                           )
                           VALUES (
                               9,
                               '2022-10-04 16:27:21',
                               10,
                               9,
                               9
                           );


-- Table: Order
DROP TABLE IF EXISTS [Order];

CREATE TABLE [Order] (
    order_id      INTEGER PRIMARY KEY,
    store_id      INTEGER NOT NULL
                          REFERENCES Store (store_id) ON DELETE CASCADE
                                                      ON UPDATE CASCADE,
    emp_id        INTEGER NOT NULL
                          REFERENCES Employees (emp_id) ON DELETE CASCADE
                                                        ON UPDATE CASCADE,
    cus_id        INTEGER NOT NULL
                          REFERENCES Customers (cus_id) ON DELETE CASCADE
                                                        ON UPDATE CASCADE,
    order_dicount DOUBLE  NOT NULL
                          DEFAULT (0),
    order_cash    DOUBLE  NOT NULL
                          DEFAULT (0),
    order_change  DOUBLE  NOT NULL
                          DEFAULT (0) 
);

INSERT INTO [Order] (
                        order_id,
                        store_id,
                        emp_id,
                        cus_id,
                        order_dicount,
                        order_cash,
                        order_change
                    )
                    VALUES (
                        1,
                        1,
                        1,
                        1,
                        0.0,
                        200.0,
                        90.0
                    );

INSERT INTO [Order] (
                        order_id,
                        store_id,
                        emp_id,
                        cus_id,
                        order_dicount,
                        order_cash,
                        order_change
                    )
                    VALUES (
                        2,
                        1,
                        1,
                        1,
                        0.0,
                        300.0,
                        70.0
                    );

INSERT INTO [Order] (
                        order_id,
                        store_id,
                        emp_id,
                        cus_id,
                        order_dicount,
                        order_cash,
                        order_change
                    )
                    VALUES (
                        3,
                        1,
                        2,
                        2,
                        0.0,
                        200.0,
                        90.0
                    );

INSERT INTO [Order] (
                        order_id,
                        store_id,
                        emp_id,
                        cus_id,
                        order_dicount,
                        order_cash,
                        order_change
                    )
                    VALUES (
                        4,
                        1,
                        2,
                        3,
                        0.0,
                        200.0,
                        95.0
                    );


-- Table: Order_items
DROP TABLE IF EXISTS Order_items;

CREATE TABLE Order_items (
    order_item_id     INTEGER PRIMARY KEY,
    order_id          INTEGER REFERENCES [Order] (order_id) ON DELETE CASCADE
                                                            ON UPDATE CASCADE,
    product_id        INTEGER REFERENCES Product (product_id) ON DELETE CASCADE
                                                              ON UPDATE CASCADE,
    order_item_amount INTEGER NOT NULL,
    order_item_total  INTEGER NOT NULL
);


-- Table: Product
DROP TABLE IF EXISTS Product;

CREATE TABLE Product (
    product_id         INTEGER   PRIMARY KEY,
    product_name       TEXT (50) NOT NULL,
    product_type       TEXT (10) NOT NULL
                                 DEFAULT HCF,
    product_size       TEXT (10) DEFAULT SML
                                 NOT NULL,
    product_sweetlevel TEXT (10) NOT NULL
                                 DEFAULT (123),
    product_price      INTEGER   NOT NULL
);

INSERT INTO Product (
                        product_id,
                        product_name,
                        product_type,
                        product_size,
                        product_sweetlevel,
                        product_price
                    )
                    VALUES (
                        1,
                        'Cappuccino',
                        'HC',
                        'SML',
                        '123',
                        60
                    );

INSERT INTO Product (
                        product_id,
                        product_name,
                        product_type,
                        product_size,
                        product_sweetlevel,
                        product_price
                    )
                    VALUES (
                        2,
                        'Latte',
                        'HC',
                        'SML',
                        '123',
                        60
                    );

INSERT INTO Product (
                        product_id,
                        product_name,
                        product_type,
                        product_size,
                        product_sweetlevel,
                        product_price
                    )
                    VALUES (
                        3,
                        'Muffin',
                        '-',
                        '-',
                        '-',
                        25
                    );


-- Table: Receipt
DROP TABLE IF EXISTS Receipt;

CREATE TABLE Receipt (
    rcp_id     INTEGER  PRIMARY KEY ASC AUTOINCREMENT,
    rcp_date   DATETIME NOT NULL
                        DEFAULT (DATETIME('now', 'localtime') ),
    rcp_cash   DOUBLE   NOT NULL
                        DEFAULT (0),
    rcp_change DOUBLE   NOT NULL
                        DEFAULT (0) 
);

INSERT INTO Receipt (
                        rcp_id,
                        rcp_date,
                        rcp_cash,
                        rcp_change
                    )
                    VALUES (
                        1,
                        '2022-10-04 16:08:52',
                        9300.0,
                        67.0
                    );


-- Table: Receipt_items
DROP TABLE IF EXISTS Receipt_items;

CREATE TABLE Receipt_items (
    reitem_id           INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    reitem_product_name TEXT (50) UNIQUE,
    reitem_price        INTEGER   DEFAULT (0) 
                                  NOT NULL,
    reitem_amout        INTEGER   NOT NULL
                                  DEFAULT (0),
    reitem_total        INTEGER   DEFAULT (0) 
                                  NOT NULL,
    rcp_id              INTEGER   REFERENCES Receipt (rcp_id) ON DELETE CASCADE
                                                              ON UPDATE CASCADE,
    mt_id               INTEGER   REFERENCES Meterial (mt_id) ON DELETE CASCADE
                                                              ON UPDATE CASCADE
);

INSERT INTO Receipt_items (
                              reitem_id,
                              reitem_product_name,
                              reitem_price,
                              reitem_amout,
                              reitem_total,
                              rcp_id,
                              mt_id
                          )
                          VALUES (
                              1,
                              'Cocoa powder',
                              120,
                              8,
                              960,
                              1,
                              1
                          );

INSERT INTO Receipt_items (
                              reitem_id,
                              reitem_product_name,
                              reitem_price,
                              reitem_amout,
                              reitem_total,
                              rcp_id,
                              mt_id
                          )
                          VALUES (
                              2,
                              'Coffee beans',
                              200,
                              9,
                              1800,
                              1,
                              2
                          );

INSERT INTO Receipt_items (
                              reitem_id,
                              reitem_product_name,
                              reitem_price,
                              reitem_amout,
                              reitem_total,
                              rcp_id,
                              mt_id
                          )
                          VALUES (
                              3,
                              'Thai tea powder',
                              130,
                              6,
                              780,
                              1,
                              3
                          );

INSERT INTO Receipt_items (
                              reitem_id,
                              reitem_product_name,
                              reitem_price,
                              reitem_amout,
                              reitem_total,
                              rcp_id,
                              mt_id
                          )
                          VALUES (
                              4,
                              'Green tea powder',
                              220,
                              9,
                              1980,
                              1,
                              4
                          );

INSERT INTO Receipt_items (
                              reitem_id,
                              reitem_product_name,
                              reitem_price,
                              reitem_amout,
                              reitem_total,
                              rcp_id,
                              mt_id
                          )
                          VALUES (
                              5,
                              'Milk',
                              880,
                              8,
                              880,
                              1,
                              5
                          );

INSERT INTO Receipt_items (
                              reitem_id,
                              reitem_product_name,
                              reitem_price,
                              reitem_amout,
                              reitem_total,
                              rcp_id,
                              mt_id
                          )
                          VALUES (
                              6,
                              'Creamer',
                              98,
                              7,
                              686,
                              1,
                              6
                          );

INSERT INTO Receipt_items (
                              reitem_id,
                              reitem_product_name,
                              reitem_price,
                              reitem_amout,
                              reitem_total,
                              rcp_id,
                              mt_id
                          )
                          VALUES (
                              7,
                              'Sugar',
                              110,
                              7,
                              770,
                              1,
                              7
                          );

INSERT INTO Receipt_items (
                              reitem_id,
                              reitem_product_name,
                              reitem_price,
                              reitem_amout,
                              reitem_total,
                              rcp_id,
                              mt_id
                          )
                          VALUES (
                              8,
                              'Condensed milk',
                              25,
                              9,
                              225,
                              1,
                              8
                          );

INSERT INTO Receipt_items (
                              reitem_id,
                              reitem_product_name,
                              reitem_price,
                              reitem_amout,
                              reitem_total,
                              rcp_id,
                              mt_id
                          )
                          VALUES (
                              9,
                              'Syrup',
                              128,
                              9,
                              1152,
                              1,
                              9
                          );


-- Table: Salary
DROP TABLE IF EXISTS Salary;

CREATE TABLE Salary (
    slr_id    INTEGER  PRIMARY KEY ASC AUTOINCREMENT,
    slr_date  DATETIME NOT NULL
                       DEFAULT (DATETIME('now', 'localtime') ),
    slr_perh  DOUBLE   DEFAULT (45) 
                       NOT NULL,
    str_hour  INTEGER  DEFAULT (0),
    slr_total DOUBLE   NOT NULL
                       DEFAULT (0),
    emp_id    INTEGER  REFERENCES Employees (emp_id) ON DELETE CASCADE
                                                     ON UPDATE CASCADE
);

INSERT INTO Salary (
                       slr_id,
                       slr_date,
                       slr_perh,
                       str_hour,
                       slr_total,
                       emp_id
                   )
                   VALUES (
                       1,
                       '2022-10-04 16:05:44',
                       45.0,
                       112,
                       5040.0,
                       1
                   );

INSERT INTO Salary (
                       slr_id,
                       slr_date,
                       slr_perh,
                       str_hour,
                       slr_total,
                       emp_id
                   )
                   VALUES (
                       2,
                       '2022-10-04 16:06:16',
                       45.0,
                       112,
                       5040.0,
                       2
                   );

INSERT INTO Salary (
                       slr_id,
                       slr_date,
                       slr_perh,
                       str_hour,
                       slr_total,
                       emp_id
                   )
                   VALUES (
                       3,
                       '2022-10-04 16:06:29',
                       45.0,
                       112,
                       5040.0,
                       3
                   );


-- Table: Stock
DROP TABLE IF EXISTS Stock;

CREATE TABLE Stock (
    stock_id               INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    stock_product_name     TEXT (50) UNIQUE,
    stock_product_category TEXT (50) NOT NULL,
    stock_product_unit     TEXT (50) NOT NULL,
    stock_amount_in        INTEGER   NOT NULL
                                     DEFAULT (0),
    stock_amount_out       INTEGER   NOT NULL
                                     DEFAULT (0),
    stock_amount_balance   INTEGER   NOT NULL
                                     DEFAULT (0) 
);

INSERT INTO Stock (
                      stock_id,
                      stock_product_name,
                      stock_product_category,
                      stock_product_unit,
                      stock_amount_in,
                      stock_amount_out,
                      stock_amount_balance
                  )
                  VALUES (
                      1,
                      'Cocoa powder',
                      'drink',
                      'bag',
                      10,
                      8,
                      2
                  );

INSERT INTO Stock (
                      stock_id,
                      stock_product_name,
                      stock_product_category,
                      stock_product_unit,
                      stock_amount_in,
                      stock_amount_out,
                      stock_amount_balance
                  )
                  VALUES (
                      2,
                      'Coffee beans',
                      'drink',
                      'bag',
                      10,
                      9,
                      1
                  );

INSERT INTO Stock (
                      stock_id,
                      stock_product_name,
                      stock_product_category,
                      stock_product_unit,
                      stock_amount_in,
                      stock_amount_out,
                      stock_amount_balance
                  )
                  VALUES (
                      3,
                      'Thai tea powder',
                      'drink',
                      'bag',
                      10,
                      6,
                      4
                  );

INSERT INTO Stock (
                      stock_id,
                      stock_product_name,
                      stock_product_category,
                      stock_product_unit,
                      stock_amount_in,
                      stock_amount_out,
                      stock_amount_balance
                  )
                  VALUES (
                      4,
                      'Green tea powder',
                      'drink',
                      'bag',
                      10,
                      9,
                      1
                  );

INSERT INTO Stock (
                      stock_id,
                      stock_product_name,
                      stock_product_category,
                      stock_product_unit,
                      stock_amount_in,
                      stock_amount_out,
                      stock_amount_balance
                  )
                  VALUES (
                      5,
                      'Milk',
                      'drink',
                      'gallon',
                      10,
                      8,
                      2
                  );

INSERT INTO Stock (
                      stock_id,
                      stock_product_name,
                      stock_product_category,
                      stock_product_unit,
                      stock_amount_in,
                      stock_amount_out,
                      stock_amount_balance
                  )
                  VALUES (
                      6,
                      'Creamer',
                      'drink',
                      'bag',
                      10,
                      7,
                      3
                  );

INSERT INTO Stock (
                      stock_id,
                      stock_product_name,
                      stock_product_category,
                      stock_product_unit,
                      stock_amount_in,
                      stock_amount_out,
                      stock_amount_balance
                  )
                  VALUES (
                      7,
                      'Sugar',
                      'drink',
                      'bag',
                      10,
                      7,
                      3
                  );

INSERT INTO Stock (
                      stock_id,
                      stock_product_name,
                      stock_product_category,
                      stock_product_unit,
                      stock_amount_in,
                      stock_amount_out,
                      stock_amount_balance
                  )
                  VALUES (
                      8,
                      'Condensed milk',
                      'drink',
                      'can',
                      10,
                      9,
                      1
                  );

INSERT INTO Stock (
                      stock_id,
                      stock_product_name,
                      stock_product_category,
                      stock_product_unit,
                      stock_amount_in,
                      stock_amount_out,
                      stock_amount_balance
                  )
                  VALUES (
                      9,
                      'Syrup',
                      'drink',
                      'bottle',
                      10,
                      9,
                      1
                  );


-- Table: Store
DROP TABLE IF EXISTS Store;

CREATE TABLE Store (
    store_id       INTEGER   PRIMARY KEY,
    store_name     TEXT (50) NOT NULL,
    store_building TEXT (50) NOT NULL,
    store_faculty  TEXT (50) NOT NULL,
    store_tel      TEXT (10) NOT NULL
);

INSERT INTO Store (
                      store_id,
                      store_name,
                      store_building,
                      store_faculty,
                      store_tel
                  )
                  VALUES (
                      1,
                      'D-coffee',
                      'medical science',
                      'Allied Health Sciences, Burapha University',
                      '0953325290'
                  );


-- Table: Worktime
DROP TABLE IF EXISTS Worktime;

CREATE TABLE Worktime (
    wt_id       INTEGER  PRIMARY KEY ASC AUTOINCREMENT,
    wt_checkin  DATETIME NOT NULL
                         DEFAULT (DATETIME('now', 'localtime') ),
    wt_checkout DATETIME DEFAULT (DATETIME('now', 'localtime') ) 
                         NOT NULL,
    wt_hour     INTEGER  NOT NULL
                         DEFAULT (0),
    wt_ot       INTEGER  DEFAULT (0) 
                         NOT NULL,
    emp_id      INTEGER  REFERENCES Employees (emp_id) ON DELETE CASCADE
                                                       ON UPDATE CASCADE
);

INSERT INTO Worktime (
                         wt_id,
                         wt_checkin,
                         wt_checkout,
                         wt_hour,
                         wt_ot,
                         emp_id
                     )
                     VALUES (
                         1,
                         '2022-10-04 15:54:57',
                         '2022-10-04 15:54:22',
                         0,
                         0,
                         1
                     );

INSERT INTO Worktime (
                         wt_id,
                         wt_checkin,
                         wt_checkout,
                         wt_hour,
                         wt_ot,
                         emp_id
                     )
                     VALUES (
                         2,
                         '2022-10-04 15:54:57',
                         '2022-10-04 15:54:57',
                         0,
                         0,
                         1
                     );

INSERT INTO Worktime (
                         wt_id,
                         wt_checkin,
                         wt_checkout,
                         wt_hour,
                         wt_ot,
                         emp_id
                     )
                     VALUES (
                         3,
                         '2022-10-04 15:55:30',
                         '2022-10-04 15:55:30',
                         0,
                         0,
                         3
                     );

INSERT INTO Worktime (
                         wt_id,
                         wt_checkin,
                         wt_checkout,
                         wt_hour,
                         wt_ot,
                         emp_id
                     )
                     VALUES (
                         4,
                         '2022-10-04 15:55:30',
                         '2022-10-04 15:55:30',
                         0,
                         0,
                         3
                     );

INSERT INTO Worktime (
                         wt_id,
                         wt_checkin,
                         wt_checkout,
                         wt_hour,
                         wt_ot,
                         emp_id
                     )
                     VALUES (
                         5,
                         '2022-10-04 15:55:30',
                         '2022-10-04 15:55:30',
                         0,
                         0,
                         2
                     );

INSERT INTO Worktime (
                         wt_id,
                         wt_checkin,
                         wt_checkout,
                         wt_hour,
                         wt_ot,
                         emp_id
                     )
                     VALUES (
                         6,
                         '2022-10-04 15:55:30',
                         '2022-10-04 15:55:30',
                         0,
                         0,
                         2
                     );


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
